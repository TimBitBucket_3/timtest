﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TimTest.Infrastructure.Extensions.Abstract
{
    public interface IPagedList<T> : IList<T>
    {
        int CurrentPage { get; }
        int PagesCount { get; }

        IPagedList<T> Create(IEnumerable<T> source, int pageIndex, int pageSize);
    }
}
