﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TimTest.Infrastructure.Extensions.Abstract;

namespace TimTest.Infrastructure.Extensions
{
    public class PagedList<T> : List<T>, IPagedList<T>
    {
        public int PagesCount { get; private set; }
        public int CurrentPage { get; private set; }

        public PagedList(IEnumerable<T> items, int pageSize, int pageIndex = 1)
        {
            CurrentPage = pageIndex;
            PagesCount = (items.Count() / pageSize);

            if (pageIndex > 1)
            {
                base.AddRange(items.Skip(pageSize * (pageIndex - 1)).Take(pageSize));
            }
            else
                this.AddRange(items.Take(pageSize));
        }

        public IPagedList<T> Create(IEnumerable<T> source, int pageSize, int pageIndex)
        {
            if (pageSize == 0) throw new ArgumentException("pageSize is not specified");

            return new PagedList<T>(this.Take(pageSize).ToList(), pageSize, pageIndex);
        }
    }

    public static class CollectionExtensions
    {
        public static IPagedList<T> ToPagedList<T>(this IEnumerable<T> collection, int pageSize, int pageIndex)
        {
            return new PagedList<T>(collection, pageSize, pageIndex);
        }
    }
}
