﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TimTest.Infrastructure.Templates
{
    public class PMCSearchResponse
    {
        public string Title { get; set; }
        public string DatePublished { get; set; }
        public string Authors { get; set; }
    }
}
