﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TimTest.Models;

namespace TimTest.Infrastructure.Validators
{
    public class SearchPublicationModelsValidator : AbstractValidator<SearchPublicationModels>
    {
        public SearchPublicationModelsValidator()
        {
            base.RuleFor(d => d.Query)
                .NotEmpty()
                .WithMessage("Query cannot be empty");
        }
    }
}
