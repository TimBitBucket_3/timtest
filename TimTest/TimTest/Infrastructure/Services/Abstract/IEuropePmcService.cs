﻿using PMC;
using System.Collections.Generic;
using TimTest.Infrastructure.Templates;
using TimTest.Models;

namespace TimTest.Infrastructure.Services.Abstract
{
    public interface IEuropePmcService
    {
        IEnumerable<PMCSearchResponse> GetPublications(string query, int page);
    }
}
