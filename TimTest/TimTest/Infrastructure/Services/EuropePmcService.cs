﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PMC;
using TimTest.Infrastructure.Services.Abstract;
using TimTest.Infrastructure.Templates;
using TimTest.Models;

namespace TimTest.Infrastructure.Services
{
    public class EuropePmcService : IEuropePmcService
    {
        private WSCitationImpl service = new WSCitationImplClient();

        public IEnumerable<PMCSearchResponse> GetPublications(string query, int page)
        {
            searchPublicationsResponse1 response = service.searchPublicationsAsync(new searchPublicationsRequest()
            {
                queryString = query,
                sort = "cited desc",
                resultType = "core",
                cursorMark = "*",
                pageSize = (25 * page).ToString()
            })
            .Result;

            if (response.@return != null)
                return _MapResult(response.@return.resultList);

            return null;
        }

        private IEnumerable<PMCSearchResponse> _MapResult(IEnumerable<result> results)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var x in results)
            {
                if (x.authorList != null)
                    foreach (var author in x.authorList)
                        sb.Append(author.fullName + " ");

                yield return new PMCSearchResponse()
                {
                    DatePublished = x.firstPublicationDate,
                    Authors = sb.ToString().TrimEnd(),
                    Title = x.title
                };

                sb.Clear();
            }
        }
    }
}
