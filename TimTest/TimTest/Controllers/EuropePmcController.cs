﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PMC;
using TimTest.Controllers.Abstract;
using TimTest.Infrastructure.Extensions;
using TimTest.Infrastructure.Services;
using TimTest.Infrastructure.Services.Abstract;
using TimTest.Infrastructure.Validators;
using TimTest.Models;

namespace TimTest.Controllers
{
    public class EuropePmcController : BaseController
    {
        private IEuropePmcService _europePmcService = new EuropePmcService();

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ReadPublications(SearchPublicationModels inModel)
        {
            var validator = new SearchPublicationModelsValidator().Validate(inModel);
            var outModel = new ReadPublications(inModel.Page);

            if (validator.IsValid)
            {
                outModel.Publications = _europePmcService
                    .GetPublications(inModel.Query, inModel.Page)
                    .ToPagedList(25, inModel.Page);

                if (outModel.Publications.Count() == 0)
                    outModel.InfoMessages.Add("No publications was found");
            }
            else
                outModel.ErrorMessages = base.GetValidatorMessages(validator.Errors).ToList();

            return View(outModel);
        }
    }
}
