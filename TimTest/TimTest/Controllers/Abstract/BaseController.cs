﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;

namespace TimTest.Controllers.Abstract
{
    public abstract class BaseController : Controller
    {
        protected IEnumerable<string> GetValidatorMessages(IEnumerable<ValidationFailure> validationResults)
        {
            foreach (var error in validationResults.Select(w => w.ErrorMessage))
                yield return error;
        }
    }
}
