﻿using PMC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TimTest.Infrastructure.Extensions.Abstract;
using TimTest.Infrastructure.Templates;
using TimTest.Models.Abstract;

namespace TimTest.Models
{
    public class SearchPublicationModels : Layout
    {
        public SearchPublicationModels()
        {
            Page = 1;
        }

        public string Query { get; set; }
        public int Page { get; set; }
    }

    public class ReadPublications : Layout
    {
        public ReadPublications()
        {

        }

        public ReadPublications(int page)
        {
            Page = page;
        }

        public int Page { get; set; }
        public string Query { get; set; }

        public IPagedList<PMCSearchResponse> Publications { get; set; }
    }
}
