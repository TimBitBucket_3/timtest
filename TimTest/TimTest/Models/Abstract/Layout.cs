﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TimTest.Models.Abstract
{
    public abstract class Layout
    {
        public Layout()
        {
            ErrorMessages = new List<string>();
            SuccessMessages = new List<string>();
            InfoMessages = new List<string>();
        }

        public ICollection<string> SuccessMessages { get; set; }
        public ICollection<string> InfoMessages { get; set; }
        public ICollection<string> ErrorMessages { get; set; }
    }
}
