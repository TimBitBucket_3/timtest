using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace TimTests
{
    [TestFixture]
    public class SearchTests
    {
        [Test]
        [TestCase("Cyclosporin dermatology Covid-19")]
        public void ResultSearch(string param)
        {
            var options = new FirefoxOptions();
            options.AcceptInsecureCertificates = true;
            var driver = new FirefoxDriver(options);

            driver.Navigate().GoToUrl("https://localhost:44321/EuropePmc/Index");
            driver.FindElementByClassName("form-control").SendKeys(param);
            driver.FindElementByClassName("btn").Click();

            if (driver.Url == "https://localhost:44321/EuropePMC/ReadPublications")
            {
                var trs = driver.FindElementsByTagName("tr");
                Assert.AreEqual(25, (trs.Count() - 1)); // -1 because of table header
                driver.Close();
            }
        }

        [Test]
        [TestCase("Kdifn324234iohsdf")]
        public void ResultSearchFalse(string param)
        {
            var options = new FirefoxOptions();
            options.AcceptInsecureCertificates = true;
            var driver = new FirefoxDriver(options);

            driver.Navigate().GoToUrl("https://localhost:44321/EuropePmc/Index");
            driver.FindElementByClassName("form-control").SendKeys(param);
            driver.FindElementByClassName("btn").Click();

            if (driver.Url == "https://localhost:44321/EuropePMC/ReadPublications")
            {
                var alert = driver.FindElementByClassName("alert-primary");
                if (alert != null)
                    Assert.AreEqual("No publications was found", alert.Text);

                driver.Close();
            }
        }
    }
}