﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimTests.Helpers
{
    public class User
    {
        public string Name { get; set; }
        public string Lastname { get; set; }
        public int Age { get; set; }
    }
}
