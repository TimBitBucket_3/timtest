﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit;
using NUnit.Framework;
using TimTest.Infrastructure.Extensions;
using TimTest.Infrastructure.Extensions.Abstract;
using TimTests.Helpers;

namespace TimTests
{
    [TestFixture]
    class IPagedListTests
    {
        [Test]
        public void GetLastPageItems()
        {
            List<string> list = new List<string>();

            // 10
            list.Add("Jan");
            list.Add("Michał");
            list.Add("Marta");
            list.Add("Marcin");
            list.Add("Agnieszka");
            list.Add("Zbigniew");
            list.Add("Mateusz");
            list.Add("Krystian");
            list.Add("Marzena");
            list.Add("Bonifacy");

            list.Add("Zygfryd");
            list.Add("Roman");

            IPagedList<string> pagedList = list.ToPagedList(5, 3);

            Assert.AreEqual(2, pagedList.Count);
        }

        [Test]
        public void OneItemResult()
        {
            List<string> list = new List<string>();
            list.Add("Jan");

            IPagedList<string> pagedList = list.ToPagedList(25, 1);
            Assert.AreEqual(1, pagedList.Count);
        }
    }
}
